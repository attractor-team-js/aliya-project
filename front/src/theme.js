import {createTheme} from "@mui/material/styles";


const theme = createTheme({

    components: {
        // Name of the component
        MuiButton: {
            styleOverrides: {
                // Name of the slot
                root: {
                    // Some CSS
                    fontSize: '1rem',
                    color: 'black',
                    backgroundColor:'white'
                },
            },
        },
        MuiTextField: {
            styleOverrides: {
              variant: 'filled',
                  fullWidth: true
            },
        },
    },

// components: {
//     MuiTextField: {
//         variant: 'filled',
//         fullWidth: true,
//     },
//     MuiButton: {
//         label: {
//             color: "#f1f1f1",
//         },
//     },
// },
//     buttonProgress: {
//         position: 'absolute',
//         top
// :
// '50%',
//     left
// :
// '50%',
//     marginTop
// :
// '-12px',
//     marginLeft
// :
// '-12px',
// },


})
;

export default theme;
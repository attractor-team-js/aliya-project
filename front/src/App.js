import Layout from "./components/Layout/Layout";
import {Route, Routes} from "react-router-dom";
import Home from "./containers/Home/Home";
import {addPlayer, addService, root} from "./paths";
import ServiceAdmin from "./containers/ServiceAdmin/ServiceAdmin";
import AddPlayerAdmin from "./components/AddPlayerAdmin/AddPlayerAdmin";


const App = () => {
    return (
        <Layout>
            <Routes>
                <Route path={root} element={<Home/>}/>
                <Route path={addService} element={<ServiceAdmin/>}/>
                {/*<Route path={addPlayer} element={<AddPlayerAdmin/>}/>*/}
            </Routes>
         </Layout>
    );
};

export default App;

import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: '100%',
    background: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    zIndex: 22,
};

const ModalView = ({children, open, onClose}) => {

    return (
        <div sx={{width: '100%'}}>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={onClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <Button onClick={onClose} sx={{color: 'black'}}> Закрыть</Button>
                        {children}
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
};

export default ModalView;

import React from 'react';

const Header = () => {
    return (
            <div className="js-sticky">
                <div className="fh5co-main-nav">
                    <div className="container-fluid">
                        <div className="fh5co-menu-1">
                            <a href="#" data-nav-section="home">Главная </a>
                            <a href="#" data-nav-section="about">О нас </a>
                            <a href="#" data-nav-section="features">Услуги</a>
                        </div>
                        <div className="fh5co-logo">
                            <a href="#">
                                <img src='../images/logo.png'
                                     style={{width:'50px', height: '50px'}}
                                />
                            </a>
                        </div>
                        <div className="fh5co-menu-2">
                            <a href="#" data-nav-section="menu">Отзывы</a>
                            <a href="#" data-nav-section="events">Events</a>
                            <a href="#" data-nav-section="reservation">Reservation</a>
                        </div>
                    </div>
                </div>
            </div>
    );
};

export default Header;
import React from 'react';
import {CssBaseline} from "@mui/material";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <main>

                    {children}
            </main>
        </>
    );
};

export default Layout;
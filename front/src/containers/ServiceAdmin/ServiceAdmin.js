import React, {useState} from "react";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../../components/UI/FileInput/FileInput";
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid} from "@mui/material";
import {makeStyles} from "@mui/styles";
import theme from "../../theme";
import {addServiceRequest, fetchServiceRequest} from "../../store/actions/serviceActions";

const useStyles = makeStyles(theme => ({
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    container:{
        width: "90%",
        margin: "0 auto",
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
                width: '60%',
             },
        [theme.breakpoints.up('md')]: {
            width: '50%',
        },
    },
}));

const ServiceAdmin = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.services.createLoading);
    const error = useSelector(state => state.services.createError);

    const [newService, setNewService] = useState({
        title: "",
        image: null,
        description: "",
        price:"",
    });


    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(newService).forEach(key => {
            formData.append(key, newService[key]);
        });

        dispatch(addServiceRequest(formData));
        dispatch(fetchServiceRequest());
        setNewService({
            title: "",
            image: null,
            description: "",
            price:""
        })
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value=e.target.value;
        setNewService(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setNewService(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
           <Container>
               <Grid
                   container
                   direction="column"
                   spacing={2}
                   component="form"
                   autoComplete="off"
                   onSubmit={submitFormHandler}
                   style={theme.container}
                   noValidate
               >
                   <h3 style={theme.title}>Добавить в список услуг</h3>
                   <FormElement
                       required
                       label="Название"
                       name="title"
                       value={newService.title}
                       onChange={inputChangeHandler}
                       error={getFieldError('title')}
                       fullWidth ={true}
                   />

                   <FormElement
                       required
                       label="Описание"
                       name="description"
                       value={newService.description}
                       onChange={inputChangeHandler}
                       error={getFieldError('description')}
                       fullWidth = {true}
                   />

                   <FormElement
                       required
                       label="Цена"
                       name="price"
                       value={newService.price}
                       onChange={inputChangeHandler}
                       error={getFieldError('price')}
                       fullWidth = {true}
                   />

                   <Grid item xs>
                       <FileInput
                           required
                           label="Картинка"
                           name="image"
                           onChange={fileChangeHandler}
                           error={getFieldError('image')}
                           fullWidth = {true}
                       />
                   </Grid>

                   <Grid item xs={12}>
                       <ButtonWithProgress
                           type="submit"
                           fullWidth
                           variant="contained"
                           color="primary"
                           className={classes.submit}
                           loading={loading}
                           disabled={loading}
                       >
                           Добавить
                       </ButtonWithProgress>
                   </Grid>
               </Grid>
           </Container>
    );
};

export default ServiceAdmin;
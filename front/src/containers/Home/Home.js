import React, {useEffect, useState} from 'react';
import {Button, Grid} from "@mui/material";
import AddToQueueIcon from '@mui/icons-material/AddToQueue';
import ServiceAdmin from "../ServiceAdmin/ServiceAdmin";
import ModalView from "../../components/UI/ModalView/ModalView";
import {useDispatch, useSelector} from "react-redux";
import {deleteServiceRequest, fetchServiceRequest} from "../../store/actions/serviceActions";
import {apiURL} from "../../config";
import ClearIcon from '@mui/icons-material/Clear';
import Player from "../../components/Player/Player";
import Carousel from "../../components/Carousel/Carousel";
import SwiperGallery from "../../components/SwiperGallery/SwiperGallery";


const Home = () => {
    const dispatch = useDispatch();
    const services = useSelector(state => state.services.services);
    const [openModal, setOpenModal] = useState(false);
    const imgUrl = apiURL + '/uploads/';


    console.log("services", services)

    useEffect(() => {
        dispatch(fetchServiceRequest());
    }, [openModal])

    // console.log(services)
    //
    // const rows = services.reduce(function (rows, key, index) {
    //     return (index % 2 == 0 ? rows.push([key])
    //         : rows[rows.length - 1].push(key)) && rows;
    // }, []);

    return (
        <div>
            <div id="fh5co-container">
                <div id="fh5co-home" className="js-fullheight" data-section="home">
                    <div className="flexslider">
                        <div className="fh5co-overlay"/>
                        <div className="fh5co-text">
                            <div className="container">
                                <div className="row">
                                    <h1 className="to-animate">Ремонтные услуги</h1>
                                    <h2 className="to-animate">Доверьте ремонт <span> проффесионалам!</span></h2>
                                </div>
                            </div>
                        </div>
                        <ul className="slides">
                            <li style={{backgroundImage: 'url(images/slide_1.jpg)'}}
                                data-stellar-background-ratio="0.5"/>
                            <li style={{backgroundImage: 'url(images/slide_2.jpg)'}}
                                data-stellar-background-ratio="0.5"/>
                            <li style={{backgroundImage: 'url(images/slide_3.jpg)'}}
                                data-stellar-background-ratio="0.5"/>
                        </ul>
                    </div>
                </div>
                <div className="js-sticky">
                    <div className="fh5co-main-nav">
                        <div className="container-fluid">
                            <div className="fh5co-menu-1">
                                <a href="#" data-nav-section="home">Главная </a>
                                <a href="#" data-nav-section="about">О нас </a>
                                <a href="#" data-nav-section="features">Услуги</a>
                            </div>
                            <div className="fh5co-logo">
                                <a href="index.html">
                                    <img src='../images/logo.png'
                                         style={{width: '50px', height: '50px'}}
                                    />
                                </a>
                            </div>
                            <div className="fh5co-menu-2">
                                <a href="#" data-nav-section="comments">Отзывы</a>
                                <a href="#" data-nav-section="works">Наши работы</a>
                                <a href="#" data-nav-section="reservation">Reservation</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="fh5co-about" data-section="about">
                    <div className="fh5co-2col fh5co-bg to-animate-2"
                         style={{backgroundImage: 'url(images/main-about.jpg)'}}/>
                    <div className="fh5co-2col fh5co-text">
                        <h2 className="heading to-animate">О нас </h2>
                        <p className="to-animate"><span className="firstcharacter">М</span>ы компания "SIRIUS"
                            предоставляем разные ремонтные услуги. Мы предлагаем нашим клиентам недорогие цены и очень
                            подробный прайс-лист на ремонтные работы в Бишкеке. Чтобы сберечь нервы и получить достойный
                            результат без лишних хлопот и усилий – доверьте проведение ремонтных работ нашим
                            профессионалам. </p>
                        <p className="text-center to-animate"><a href="#" className="btn btn-primary btn-outline">Свяжитесь
                            с нами</a></p>
                    </div>
                </div>
                <div id="fh5co-sayings" data-section="comments">
                    <div className="container">
                        <div className="row to-animate">
                            <div className="flexslider">
                                <ul className="slides">
                                    <li>
                                        <blockquote>
                                            <p>“Соонун ремонт кылып беришти. Баасы дагы абдан жакты”</p>
                                            <p className="quote-author">— Бактыгул Идрисова (21.08.20)</p>
                                        </blockquote>
                                    </li>
                                    <li>
                                        <blockquote>
                                            <p>“Ребята просто молодцы! Знают свою работу. Долго не могли найти
                                                строителей, искали по всему Кыргызстану. Азамат байке спасибо за помощь.
                                                Ваш труд не оценим.”</p>
                                            <p className="quote-author">— Эрнис Кайыпкулов (19.05.21)</p>
                                        </blockquote>
                                    </li>
                                    <li>
                                        <blockquote>
                                            <p>“Цена и качество именно здесь!”</p>
                                            <p className="quote-author">— Бек Марс (7.06.21)</p>
                                        </blockquote>
                                    </li>
                                    <li>
                                        <blockquote>
                                            <p>“Большое спасибо вам. Теперь ремонт дома не так уж и страшен”</p>
                                            <p className="quote-author">— Баланчаев Баланча (31.10.21)</p>
                                        </blockquote>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="fh5co-featured" data-section="features">
                    <div className="container">
                        <div className="row text-center fh5co-heading row-padded">
                            <div className="col-md-8 col-md-offset-2">
                                <h2 className="heading to-animate">Наши Услуги</h2>
                            </div>
                            <Button
                                startIcon={<AddToQueueIcon/>}
                                onClick={() => setOpenModal(true)}
                            >
                                Добавить услугу!
                            </Button>
                            <ModalView
                                children={<ServiceAdmin/>}
                                open={openModal}
                                onClose={() => setOpenModal(false)}
                            />
                        </div>
                        <div className="row">
                            <div className="fh5co-grid">
                                {/*<div className="fh5co-v-half to-animate-2">*/}
                                {/*    <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/shpatlevka.jpg)'}} />*/}
                                {/*    <div className="fh5co-v-col-2 fh5co-text fh5co-special-1 arrow-left">*/}
                                {/*        <h2>Шпатлёвка</h2>*/}
                                {/*        <span className="pricing">150 сом за кв.м</span>*/}
                                {/*        <p>Если Вы хотите, чтобы Ваши стены были ровными и стильными, то Вы обратились по адресу. Шпатлевка стен и потолков. Цена 150 сом за кв.м</p>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                {services.length > 0 && services.map((row, i) => (
                                    <div className="fh5co-v-half" key={i}>
                                        <div className="fh5co-h-row-2 to-animate-2">
                                            <Button
                                                variant="text"
                                                startIcon={<ClearIcon style={{fontSize: 33}}/>}
                                                style={
                                                    {
                                                        position: "absolute",
                                                        top: '0',
                                                        right: '-20px',
                                                        zIndex: '555',
                                                        color: "black"
                                                    }}
                                                onClick={() => dispatch(deleteServiceRequest(row[0]._id))}
                                            >
                                            </Button>
                                            <div className="fh5co-v-col-2 fh5co-bg-img"
                                                 style={{backgroundImage: `url(${imgUrl}${row[0].image})`}}/>
                                            <div className="fh5co-v-col-2 fh5co-text arrow-left">
                                                <h2>{row[0].name}</h2>
                                                <span className="pricing">{row[0].price}</span>
                                                <p>{row[0].description}</p>
                                            </div>
                                        </div>
                                        {row.length > 1 && (
                                            <div className="fh5co-h-row-2 fh5co-reversed to-animate-2">
                                                <Button
                                                    variant="text"
                                                    startIcon={<ClearIcon style={{fontSize: 33}}/>}
                                                    style={{
                                                        position: "absolute",
                                                        top: '0',
                                                        right: '-20px',
                                                        zIndex: '555',
                                                        color: "black"
                                                    }}
                                                    onClick={() => dispatch(deleteServiceRequest(row[1]._id))}
                                                >
                                                </Button>
                                                <div className="fh5co-v-col-2 fh5co-bg-img"
                                                     style={{backgroundImage: `url(${imgUrl}${row[1].image})`}}/>
                                                <div className="fh5co-v-col-2 fh5co-text arrow-right">
                                                    <h2>{row[1].name}</h2>
                                                    <span className="pricing">{row[1].price}</span>
                                                    <p>{row[1].description}</p>
                                                </div>
                                            </div>
                                        )}

                                    </div>
                                ))}
                                {/*<div className="fh5co-v-half">*/}
                                {/*    <div className="fh5co-h-row-2 to-animate-2">*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/shtukaturka.jpeg)'}} />*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-text arrow-left">*/}
                                {/*            <h2>Штукатурка</h2>*/}
                                {/*            <span className="pricing">50 сом за кв.м</span>*/}
                                {/*            <p>Штукатурка в зависимости от толшины стен. Цена от 50 сом за кв.м</p>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*    <div className="fh5co-h-row-2 fh5co-reversed to-animate-2">*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/kleemoboi.jpg)'}} />*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-text arrow-right">*/}
                                {/*            <h2>Обои</h2>*/}
                                {/*            <span className="pricing">185 сом за кв.м</span>*/}
                                {/*            <p>Клеим обои разного уровня и качества</p>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                {/*<div className="fh5co-v-half">*/}
                                {/*    <div className="fh5co-h-row-2 fh5co-reversed to-animate-2">*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/ustkafel.jpg)'}} />*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-text arrow-right">*/}
                                {/*            <h2>Кафель</h2>*/}
                                {/*            <span className="pricing">200 сом за кв.м</span>*/}
                                {/*            <p>Установка кафеля обсуждется индивидуально.</p>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*    <div className="fh5co-h-row-2 to-animate-2">*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/elektr.jpg)'}} />*/}
                                {/*        <div className="fh5co-v-col-2 fh5co-text arrow-left">*/}
                                {/*            <h2>Электрические работы</h2>*/}
                                {/*            <span className="pricing">200 сом за кв.м</span>*/}
                                {/*            <p>Электрические работы в квартире а также в частном доме.</p>*/}
                                {/*        </div>*/}
                                {/*    </div>*/}
                                {/*</div>*/}

                                {/*<div className="fh5co-v-half to-animate-2">*/}
                                {/*    <div className="fh5co-v-col-2 fh5co-bg-img" style={{backgroundImage: 'url(images/painting.jpg)'}} />*/}
                                {/*    <div className="fh5co-v-col-2 fh5co-text fh5co-special-1 arrow-left">*/}
                                {/*        <h2>Покраска</h2>*/}
                                {/*        <span className="pricing">35 сом за кв.м</span>*/}
                                {/*        <p>Покраска стен у нас самые дешевые в городе Бишкек. Не верите - ПРОВЕРЬТЕ!</p>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
                <div id="fh5co-type" style={{backgroundImage: 'url(images/slide_3.jpg)'}}
                     data-stellar-background-ratio="fixed" data-section="works">
                    <div className="fh5co-overlay"/>
                    <div className="container">

                                <Player/>
                        {/*    <div className="col-md-3 to-animate">*/}
                        {/*        <div className="fh5co-type">*/}
                        {/*            <h3 className="with-icon icon-1">Fruits</h3>*/}
                        {/*            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*    <div className="col-md-3 to-animate">*/}
                        {/*        <div className="fh5co-type">*/}
                        {/*            <h3 className="with-icon icon-2">Sea food</h3>*/}
                        {/*            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*    <div className="col-md-3 to-animate">*/}
                        {/*        <div className="fh5co-type">*/}
                        {/*            <h3 className="with-icon icon-3">Vegetables</h3>*/}
                        {/*            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*    <div className="col-md-3 to-animate">*/}
                        {/*        <div className="fh5co-type">*/}
                        {/*            <h3 className="with-icon icon-4">Meat</h3>*/}
                        {/*            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}

                    </div>
                </div>
                <div id="fh5co-menus" data-section="menu">
                    <div className="container">
                        <Carousel/>
                        {/*<SwiperGallery/>*/}
                {/*        <div className="row text-center fh5co-heading row-padded">*/}
                {/*            <div className="col-md-8 col-md-offset-2">*/}
                {/*                <h2 className="heading to-animate">Food Menu</h2>*/}
                {/*                <p className="sub-heading to-animate">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*        <div className="row row-padded">*/}
                {/*            <div className="col-md-6">*/}
                {/*                <div className="fh5co-food-menu to-animate-2">*/}
                {/*                    <h2 className="fh5co-drinks">Drinks</h2>*/}
                {/*                    <ul>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_5.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Pineapple Juice</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $17.50*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_6.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Green Juice</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $7.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_7.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Soft Drinks</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_5.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Carlo Rosee Drinks</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-6">*/}
                {/*                <div className="fh5co-food-menu to-animate-2">*/}
                {/*                    <h2 className="fh5co-dishes">Steak</h2>*/}
                {/*                    <ul>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_3.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Beef Steak</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $17.50*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_4.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Tomato with Chicken</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $7.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_2.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Sausages from Italy</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_8.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Beef Grilled</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-6">*/}
                {/*                <div className="fh5co-food-menu to-animate-2">*/}
                {/*                    <h2 className="fh5co-drinks">Drinks</h2>*/}
                {/*                    <ul>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_5.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Pineapple Juice</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $17.50*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_6.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Green Juice</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $7.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_7.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Soft Drinks</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_5.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Carlo Rosee Drinks</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-6">*/}
                {/*                <div className="fh5co-food-menu to-animate-2">*/}
                {/*                    <h2 className="fh5co-dishes">Steak</h2>*/}
                {/*                    <ul>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_3.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Beef Steak</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $17.50*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_4.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Tomato with Chicken</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $7.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_2.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Sausages from Italy</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <div className="fh5co-food-desc">*/}
                {/*                                <figure>*/}
                {/*                                    <img src="images/res_img_8.jpg" className="img-responsive" alt="Free HTML5 Templates by FREEHTML5.co" />*/}
                {/*                                </figure>*/}
                {/*                                <div>*/}
                {/*                                    <h3>Beef Grilled</h3>*/}
                {/*                                    <p>Far far away, behind the word mountains.</p>*/}
                {/*                                </div>*/}
                {/*                            </div>*/}
                {/*                            <div className="fh5co-food-pricing">*/}
                {/*                                $12.99*/}
                {/*                            </div>*/}
                {/*                        </li>*/}
                {/*                    </ul>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*        <div className="row">*/}
                {/*            <div className="col-md-4 col-md-offset-4 text-center to-animate-2">*/}
                {/*                <p><a href="#" className="btn btn-primary btn-outline">More Food Menu</a></p>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                    </div>
                </div>
                {/*<div id="fh5co-events" data-section="events" style={{backgroundImage: 'url(images/slide_2.jpg)'}} data-stellar-background-ratio="0.5">*/}
                {/*    <div className="fh5co-overlay" />*/}
                {/*    <div className="container">*/}
                {/*        <div className="row text-center fh5co-heading row-padded">*/}
                {/*            <div className="col-md-8 col-md-offset-2 to-animate">*/}
                {/*                <h2 className="heading">Upcoming Events</h2>*/}
                {/*                <p className="sub-heading">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*        <div className="row">*/}
                {/*            <div className="col-md-4">*/}
                {/*                <div className="fh5co-event to-animate-2">*/}
                {/*                    <h3>Kitchen Workshops</h3>*/}
                {/*                    <span className="fh5co-event-meta">March 19th, 2016</span>*/}
                {/*                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*                    <p><a href="#" className="btn btn-primary btn-outline">Read More</a></p>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-4">*/}
                {/*                <div className="fh5co-event to-animate-2">*/}
                {/*                    <h3>Music Concerts</h3>*/}
                {/*                    <span className="fh5co-event-meta">March 19th, 2016</span>*/}
                {/*                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*                    <p><a href="#" className="btn btn-primary btn-outline">Read More</a></p>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-4">*/}
                {/*                <div className="fh5co-event to-animate-2">*/}
                {/*                    <h3>Free to Eat Party</h3>*/}
                {/*                    <span className="fh5co-event-meta">March 19th, 2016</span>*/}
                {/*                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*                    <p><a href="#" className="btn btn-primary btn-outline">Read More</a></p>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
                {/*<div id="fh5co-contact" data-section="reservation">*/}
                {/*    <div className="container">*/}
                {/*        <div className="row text-center fh5co-heading row-padded">*/}
                {/*            <div className="col-md-8 col-md-offset-2">*/}
                {/*                <h2 className="heading to-animate">Reserve a Table</h2>*/}
                {/*                <p className="sub-heading to-animate">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*        <div className="row">*/}
                {/*            <div className="col-md-6 to-animate-2">*/}
                {/*                <h3>Contact Info</h3>*/}
                {/*                <ul className="fh5co-contact-info">*/}
                {/*                    <li className="fh5co-contact-address ">*/}
                {/*                        <i className="icon-home" />*/}
                {/*                        5555 Love Paradise 56 New Clity 5655, <br />Excel Tower United Kingdom*/}
                {/*                    </li>*/}
                {/*                    <li><i className="icon-phone" /> (123) 465-6789</li>*/}
                {/*                    <li><i className="icon-envelope" />info@freehtml5.co</li>*/}
                {/*                    <li><i className="icon-globe" /> <a href="http://freehtml5.co/" target="_blank">freehtml5.co</a></li>*/}
                {/*                </ul>*/}
                {/*            </div>*/}
                {/*            <div className="col-md-6 to-animate-2">*/}
                {/*                <h3>Reservation Form</h3>*/}
                {/*                <div className="form-group ">*/}
                {/*                    <label htmlFor="name" className="sr-only">Name</label>*/}
                {/*                    <input id="name" className="form-control" placeholder="Name" type="text" />*/}
                {/*                </div>*/}
                {/*                <div className="form-group ">*/}
                {/*                    <label htmlFor="email" className="sr-only">Email</label>*/}
                {/*                    <input id="email" className="form-control" placeholder="Email" type="email" />*/}
                {/*                </div>*/}
                {/*                <div className="form-group">*/}
                {/*                    <label htmlFor="occation" className="sr-only">Occation</label>*/}
                {/*                    <select className="form-control" id="occation">*/}
                {/*                        <option>Select an Occation</option>*/}
                {/*                        <option>Wedding Ceremony</option>*/}
                {/*                        <option>Birthday</option>*/}
                {/*                        <option>Others</option>*/}
                {/*                    </select>*/}
                {/*                </div>*/}
                {/*                <div className="form-group ">*/}
                {/*                    <label htmlFor="date" className="sr-only">Date</label>*/}
                {/*                    <input id="date" className="form-control" placeholder="Date & Time" type="text" />*/}
                {/*                </div>*/}
                {/*                <div className="form-group ">*/}
                {/*                    <label htmlFor="message" className="sr-only">Message</label>*/}
                {/*                    <textarea name id="message" cols={30} rows={5} className="form-control" placeholder="Message" defaultValue={""} />*/}
                {/*                </div>*/}
                {/*                <div className="form-group ">*/}
                {/*                    <input className="btn btn-primary" defaultValue="Send Message" type="submit" />*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </div>
            <div id="fh5co-footer">
                <div className="container">
                    <div className="row row-padded">
                        <div className="col-md-12 text-center">
                            <p className="to-animate">©2022 Компания Sirius <br/>
                            </p>
                            <p className="text-center to-animate"><a href="#" className="js-gotop">Вверх</a></p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <ul className="fh5co-social">
                                <li className="to-animate-2"><a href="#"><i className="icon-facebook"/></a></li>
                                <li className="to-animate-2"><a href="#"><i className="icon-whatsapp"/></a></li>
                                <li className="to-animate-2"><a href="#"><i className="icon-instagram"/></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
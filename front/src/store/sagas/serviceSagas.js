import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import History from "../../History";
import {root} from "../../paths";
import {put, takeEvery} from "redux-saga/effects";
import {
    addServiceFailure,
    addServiceRequest,
    addServiceSuccess,
    deleteServiceFailure, deleteServiceRequest,
    deleteServiceSuccess,
    fetchOneServiceFailure,
    fetchOneServiceRequest,
    fetchOneServiceSuccess,
    fetchServiceRequest,
    fetchServiceSuccess
} from "../actions/serviceActions";



export function* addServiceSaga({payload}) {
    try {
        yield axiosApi.post( '/services', payload);
        yield put(addServiceSuccess());
        History.push(root);
        toast.success('Услуга добавлена!');

    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(addServiceFailure(error.response.data));
    }
}

export function* getServicesSagas() {
    try {
        const response = yield axiosApi.get('/services');
        yield put(fetchServiceSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchServiceRequest(error.response.data));
    }
}

export function* getOneSagas({payload}) {
    try {
        const response = yield axiosApi.get('/services/'+payload);
        yield put(fetchOneServiceSuccess(response.data));
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        }
        yield put(fetchOneServiceFailure(error.response.data));
    }
}

export function* deleteServiceSaga({payload: id}) {
    try {
        console.log("id", id)
        yield axiosApi.delete('/services/'+id);
        yield put(deleteServiceSuccess(id));
        toast.success('Успешно удален!');
    } catch (error) {
        if (!error.response) {
            toast.error(error.message);
        } else {
            toast.error(error.response?.data?.global);
        }
        yield put(deleteServiceFailure(error.response?.data));
    }
}




const serviceSaga = [
    takeEvery(addServiceRequest, addServiceSaga),
    takeEvery(fetchServiceRequest, getServicesSagas),
    takeEvery(fetchOneServiceRequest, getOneSagas),
    takeEvery(deleteServiceRequest, deleteServiceSaga),

];

export default serviceSaga;
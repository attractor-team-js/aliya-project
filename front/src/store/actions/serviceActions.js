import servicesSlice from "../slices/serviceSlices";


export const {
    addServiceRequest,
    addServiceFailure,
    addServiceSuccess,
    clearServiceError,
    fetchServiceRequest,
    fetchServiceSuccess,
    fetchServiceFailure,
    fetchOneServiceRequest,
    fetchOneServiceSuccess,
    fetchOneServiceFailure,
    deleteServiceSuccess,
    deleteServiceFailure,
    deleteServiceRequest,
} = servicesSlice.actions;
import usersSlice from "../slices/userSlice";

export const {
    registerUser,
    registerUserSuccess,
    registerUserFailure,
    loginUser,
    loginUserSuccess,
    loginUserFailure,
    clearError,
    logout,
} = usersSlice.actions;
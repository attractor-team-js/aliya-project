import {createSlice} from "@reduxjs/toolkit";

const name = 'services'
const initialState = {
    services:[],
    service:{},
    fetchLoading: false,
    createLoading: false,
    createError: null,
    fetchError: null,
    deleteLoading: false,
    deleteError:null,
}

const servicesSlice = createSlice({
    name,
    initialState,
    reducers: {
        addServiceRequest(state){
            state.createLoading = true;
        },
        addServiceSuccess(state) {
            state.createLoading = false;
            state.createError = null;
        },
        addServiceFailure(state,action ){
            state.createLoading = false;
            state.createError = action.payload;
        },
        clearServiceError(state){
            state.createError=null;
        },
        fetchServiceRequest(state){
            state.fetchLoading = true;
        },
        fetchServiceSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.services = action.payload
        },
        fetchServiceFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },
        fetchOneServiceRequest(state){
            state.fetchLoading = true;
        },
        fetchOneServiceSuccess(state, action){
            state.fetchLoading = false;
            state.fetchError = null;
            state.building = action.payload
        },
        fetchOneServiceFailure(state,action){
            state.fetchLoading = false;
            state.fetchError = action.payload;
        },
        deleteServiceRequest(state){
            state.deleteLoading = true;
        },
        deleteServiceSuccess(state,{payload:id}){
            state.deleteLoading = false;
            state.deleteError =null;
            state.services = state.services.map(ser=>ser.filter(s=>s._id!==id)).filter(k=>k.length);
        },
        deleteServiceFailure(state,{payload:error}){
            state.deleteLoading = false;
            state.deleteError = error;
        }

    }
});

export default servicesSlice;
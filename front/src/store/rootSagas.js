import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import serviceSagas from "./sagas/serviceSagas";
import playerSagas from "./sagas/playerSagas";
import carouselsSagas from "./sagas/carouselsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...serviceSagas,
        ...playerSagas,
        ...carouselsSagas,
    ]);
}
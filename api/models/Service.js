const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const ServiceSchema = new mongoose.Schema({
  title:{
    type: String,
    required: 'Название обязательно!',
  },
  description:{
    type: String,
    required: 'Описание обязательно!',
  },
  datetime:{
    type: Date,
    default: Date.now,
  },
  image: {
    type: String,
    required: 'Фото обязательно!',
  },
  price:{
    type: String,
    required: "Укажите цену!"
  }

})


ServiceSchema.plugin(idValidator);
const Service = mongoose.model('Service', ServiceSchema);
module.exports = Service;
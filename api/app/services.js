const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Service = require("../models/Service");
const {nanoid} = require("nanoid");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const services = await Service.find();
    const rows= services.reduce(function (rows, key, index) {
      return (index % 2 == 0 ? rows.push([key])
        : rows[rows.length - 1].push(key)) && rows;
    }, []);

    res.send(rows);
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const service = await Service.findById(req.params.id);
    if(!service){
      res.status(404).send({error: 'Place not found'});
    }else{
      res.send(service);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/',upload.single('image'), async (req, res) => {
  try {
    const serviceData = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      // user: req.user._id,
    };

    if(req.file) {
      serviceData.image = req.file.filename;
    }


    const service = new Service(serviceData);
    await service.save();
    res.send(service);

  } catch (error) {
    res.status(400).send(error);
    console.log(error)
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const service = await Service.findByIdAndDelete(req.params.id);
    if (service) {
      return res.send({message: `Услуга успешно удалена!.`})
    }
  } catch (error) {
    res.status(404).send(error);
  }
});





module.exports = router;